var path = require("path");
const express = require("express");
const mockAPIResponse = require("./mockAPI.js");
var cors = require("cors");

const app = express();
app.use(cors());

app.use(express.static("dist"));

console.log(__dirname);

app.get("/", function(req, res) {
  res.sendFile("dist/index.html");
});

// designates what port the app will listen to for incoming requests
app.listen(8082, function() {
  console.log("Example app listen ing on port 8082!");
});

app.get("/test", function(req, res) {
  res.send(mockAPIResponse);
});
