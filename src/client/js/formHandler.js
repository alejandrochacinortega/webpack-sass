function handleSubmit(event) {
  event.preventDefault();

  // check what text was put into the form field
  let formText = document.getElementById("name").value;
  console.log("Client ", Client);
  Client.checkForName(formText);

  console.log("::: Form Submitted :::");
  fetch("http://localhost:8082/test")
    .then(res => res.json())
    .then(function(res) {
      document.getElementById("results").innerHTML = res.message;
    })
    .catch(error => console.log("Errrr ", error));
}

export { handleSubmit };
